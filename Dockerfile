FROM alpine:3.19

RUN apk add --no-cache chromium-chromedriver

EXPOSE 9515

ENTRYPOINT ["chromedriver"]
